
FROM ubuntu
MAINTAINER safa.ouesleti@gmail.com
RUN mkdir /opt/tomcat/
RUN tomcat:8.5.47-jdk8-openjdk
RUN container -name myapp -d -p 8080:8080 tomcat:8.5.47-jdk8-openjdk
RUN stop myapp
RUN  rm myapp



FROM tomcat:8.5.47-jdk8-openjdk
COPY /webapp/target/webapp.war /usr/local/tomcat/webapps/

RUN build -t webapp-tomcat .

RUN -d -p 8080:8080 webapp-tomcat

CMD ["/opt/tomcat/bin/catalina.sh", "run"]
